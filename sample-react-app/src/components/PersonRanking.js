
import React, { Component } from 'react';
// import './../css/cssoverride.css';

const PersonRanking = () => {
  let domfrontendwidgetconfparm = null;
  if (document.getElementById('domfrontendwidgetconfparm') != null) {
    domfrontendwidgetconfparm = document.getElementById('domfrontendwidgetconfparm').value;
  }
  return (
    <div class="constainer-fluid">
      <div class="ibox">
        <div class="ibox-title">
          <h2 class="card-pf-title">
            <span>Configurable Widget</span>
          </h2>
        </div>
        <div class="ibox float-e-margins">
          <div class="ibox-content">
            <h4>Widget Config parameter:</h4>
            {domfrontendwidgetconfparm}
          </div>
        </div>
      </div>
    </div>

  )
}
export default PersonRanking;
